export class Shipper {
  id: number;
  companyName: String;
  phone: String;

  constructor(){
    this.id = 0;
    this.companyName = "";
    this.phone="";
  }
}
